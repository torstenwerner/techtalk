# This run.Dockerfile builds the base image used in Dockerfile for running the application.
# The base image should be rebuilt occasionally.
# Its main purpose is to create a smaller custom java runtime with jlink.

# docker pull openjdk:11-slim
# docker build -t registry.gitlab.com/torstenwerner/techtalk/run -f run.Dockerfile .
# docker push registry.gitlab.com/torstenwerner/techtalk/run

FROM openjdk:11-slim AS build
# see https://github.com/docker-library/openjdk/issues/217 for the strip workaround
RUN jlink --add-modules java.base,java.sql,java.desktop,java.management,java.naming --output /opt/java && \
    apt-get update && \
    apt-get install -y binutils && \
    strip -p --strip-unneeded /opt/java/lib/server/libjvm.so

FROM debian:stretch-slim
RUN mkdir /app
WORKDIR /app
COPY --from=build /opt/java /opt/java/
EXPOSE 5000
CMD /opt/java/bin/java -jar app.jar
