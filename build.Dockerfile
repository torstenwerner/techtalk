# This build.Dockerfile builds the base image used in Dockerfile for building the application.
# The base image should be rebuilt occasionally.
# Its main purpose is to pre-cache the gradle dependencies to avoid downloading all of them for every build.

# docker pull openjdk:11-slim
# docker build -t registry.gitlab.com/torstenwerner/techtalk/build -f build.Dockerfile .
# docker push registry.gitlab.com/torstenwerner/techtalk/build

FROM openjdk:11-slim AS build
RUN mkdir /app
WORKDIR /app
COPY . ./
RUN ./gradlew -i build

FROM openjdk:11-slim
RUN mkdir /app
WORKDIR /app
COPY --from=build /root/.gradle /root/.gradle/
