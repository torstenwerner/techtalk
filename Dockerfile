# Builds the application from source and creates a docker image.
# The first stage builds the application. The second stage packages the runtime image.

# docker build -t registry.gitlab.com/torstenwerner/techtalk .
# docker run -it --rm -p5000:5000 registry.gitlab.com/torstenwerner/techtalk
# docker push registry.gitlab.com/torstenwerner/techtalk

# The base image gets created by build.Dockerfile.
FROM registry.gitlab.com/torstenwerner/techtalk/build AS build
COPY . ./
RUN ./gradlew -i build

FROM registry.gitlab.com/torstenwerner/techtalk/run
COPY --from=build /app/build/libs/techtalk-*.jar app.jar
