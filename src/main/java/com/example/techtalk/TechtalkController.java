package com.example.techtalk;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class TechtalkController {
    @GetMapping("/")
    public Mono<String> hello() {
        return Mono.just("Hello Gitlab! Hello Westernacher! Your branch.");
    }
}
