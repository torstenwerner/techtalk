# gitlab techtalk

To build and run the docker image execute:

    docker build -t techtalk .
    docker run --rm -it -p5000:5000 techtalk
    
The port 5000 is needed for the automatic gitlab deployments which expect this port.
